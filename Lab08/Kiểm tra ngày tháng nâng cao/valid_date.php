<html>
    <head>
        <title>Result</title>
    </head>
    <body>
        <?php
        $date = $_POST["date"];
        
        $month = '0[[:digit:]]|1[0-2]';
        $day = '0[1-9]|[1-2][[:digit:]]|3[0-1]';
        $year = '2[[:digit:]]{3}';

        function check_date($month, $day, $year){
            $check = 1;
            if($month==4 || $month==6 || $month==9 || $month== 11){
                if($day == 31) $check= 0;
            }
            elseif($month == 2){
                if($day == 30 || $day == 31) $check= 0;
                elseif($day == 29){
                    if($year % 4 != 0)  $check= 0;
                    elseif($year % 100 == 0 && $year % 400 !=0)  $check= 0;
                }
            }
            return $check;
        }

        if(preg_match("/^($month)\/($day)\/($year)$/", $date)){
            list($mon, $day, $year) = preg_split('/\//', $date);
            $check = check_date($mon, $day, $year);
            if($check ==1){
                print "Got valid date= $date <br>";
            }
            else print "Invalid date= $date <br>";
        } else {
            print "Invalid date= $date <br>";
        }
        ?>
    </body>
</html>