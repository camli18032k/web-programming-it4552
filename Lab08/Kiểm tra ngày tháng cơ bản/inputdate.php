<html>
    <head>
        <title>Result</title>
    </head>
    <body>
        <?php
        $date = $_POST["date"];
        
        $month = '0[[:digit:]]|1[0-2]';
        $day = '0[1-9]|[1-2][[:digit:]]|3[0-1]';
        $year = '2[[:digit:]]{3}';

        if(preg_match("/^($month)\/($day)\/($year)$/", $date)){
            print "Got valid date= $date <br>";
        } else {
            print "Invalid date= $date <br>";
        }
        ?>
    </body>
</html>