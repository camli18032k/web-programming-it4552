<html>
    <head>
        <title>Bai 0</title>
    </head>
    <body>
        <?php
        $host = 'localhost';
        $user = 'root';
        $passwd = '';  
        $database = 'business_service';
        $table1 = 'Businesses';
        $table2 = 'Categories';
        $table3 = 'Biz_Categories';
        
        // tao ket noi
        $connect = mysqli_connect($host,$user,$passwd);
        // kiem tra ket noi
        if(!$connect){
            die("Ket noi that bai: " . mysqli_connect_error());
        }
        // tao database
        $sql = "CREATE DATABASE $database";

        // thuc thi cau lenh
        if(mysqli_query($connect, $sql)){
            echo "Tao database $database thanh cong!"; 
            echo "<br>";
        } else {
            echo "Tao database $database that bai: " . mysqli_error($connect);
            echo "<br>";
        }
        $connect_db = mysqli_connect($host, $user, $passwd, $database);
        $sql1 = "CREATE TABLE $table1 (
            Business_id int(11) auto_increment primary key,
            Name varchar(50) not null,
            Address varchar(50),
            City varchar(50),
            Telephone varchar(10),
            URL varchar(50)
        )";
        $sql2 = "CREATE TABLE $table2 (
            Category_id varchar(10) not null primary key,
            Title varchar(50) not null,
            Description varchar(50)
        )";            
        $sql3 = "CREATE TABLE $table3 (
            Business_id int(11) auto_increment,
            Category_id varchar(10),
            primary key (Business_id, Category_id),
            foreign key (Business_id) references Businesses(Business_id),
            foreign key (Category_id) references Categories(Category_id)
        )" ; 
        if (mysqli_query($connect_db, $sql1) && mysqli_query($connect_db, $sql2) && mysqli_query($connect_db, $sql3))  {
            echo "Tao 3 bang $table1, $table2, $table3 thanh cong!";
        } else {
            echo "Loi tao bang: " . mysqli_error($connect_db);
        }
        mysqli_close($connect);
        ?>
    </body>
</html>