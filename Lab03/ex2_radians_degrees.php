<html>
    <body>
        <?php 
        $number = $_POST["number"];
        $type = $_POST["choose"];
        ?>
        <form method = "post">
            <p>Enter the number: <input type = "text" name = "number" value= <?php echo $number?>></p>
            <p>Choose:</p>
            <p><input type = "radio" name = "choose" value = "rads_deg" <?php if($type == "rads_deg") echo "checked"?>>Radians -> Degrees</p>
            <p><input type = "radio" name = "choose" value = "deg_rads" <?php if($type == "deg_rads") echo "checked"?>>Degrees -> Radians</p>            
            <p><input type="submit" value = "Submit"></p>
        </form>
        <p>
            Result: 
            <?php 
            $result = convert($type, $number);
            print $result;
            function convert($type, $number){
                if($type == "rads_deg"){
                    $result = $number * 180/ pi();
                }
                else $result = $number * pi()/ 180;
                return $result;
            }
            ?>
        </p>
    </body>        
</html>