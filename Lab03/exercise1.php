<html>
    <body>
    <?php 
        $name = $_POST["name"];
        $day = $_POST["day"];
        $month = $_POST["month"];
        $year = $_POST["year"];
        $hour = $_POST["hour"];
        $minute = $_POST["minute"];
        $second = $_POST["second"];
        ?>
        <p>Enter your name and select date and time for the appointment</p>
        <form method="post">
        <p>Your name: <input type="text" id="name" name="name" value= <?php print $name ?>></p>
        <p>Date:      
        <select name = "day">
         <?php for($i=1; $i<=31; $i++){
           if($i == $day) select_option($i);
           else print_option($i);
         }   
         ?>
        </select>
        <select name= "month">
         <?php for($i=1; $i<=12; $i++){
           if($i == $month) select_option($i);
           else print_option($i);
         }   
         ?>
        </select>
        <select name= "year">
         <?php for($i=2000; $i<=2030; $i++){
           if($i == $year) select_option($i);
           else print_option($i);
         }   
         ?>
        </select>
       </p>
        <p>Time:  
        <select name="hour">
         <?php for($i=0; $i<24; $i++){
           if($i == $hour) select_option($i);
           else print_option($i);
         }   
         ?>
        </select>
        <select name = "minute">
         <?php for($i=0; $i<=60; $i++){
           if($i == $minute) select_option($i);
           else print_option($i);
         }   
         ?>
        </select>
        <select name= "second">
         <?php for($i=0; $i<=60; $i++){
           if($i == $second) select_option($i);
           else print_option($i);
         }   
         ?>
        </select>     
       </p>
        <input type="submit" value= "Submit">
        <input type= "reset" value= "Reset">
      </form>      
       <p>
            Hi 
            <?php
            print("$name!");
             ?>
              </p>
        <p>
            You have choose to have an appointment on
            <?php 
            print("$hour:$minute:$second, $day/$month/$year");
            ?>
        </p>
        <p>
          <?php 
          $check = check_valid_day($day,$month,$year);
          if($check == 0){
            print("$day/$month/$year doesn't exist!");
          }
          ?>
        </p> 
        <p>
          More information
        </p>   
        <p>
          <?php 
          if($hour > 12){
            $hour_pm = $hour - 12;
            print("In 12 hours, the time and date is $hour_pm:$minute:$second PM, $day/$month/$year");
          }
          ?>
        </p>
        <p>
          <?php 
          $days_of_month = days_of_month($month, $year); 
          print("This month has $days_of_month days!");
          ?>
        </p>              
        <?php
        function print_option($ten_option){
            print("<option value = \"$ten_option\">$ten_option</option>");
        }
        function select_option($ten_option){
          print("<option value = \"$ten_option\" selected>$ten_option</option>");
        } 
        function check_valid_day($day,$month,$year){
          $check = 1;
          if($day== 31){
            if($month==2 || $month==4||$month==6||$month==9||$month== 11) $check = 0;
          }
          if($day == 30 && $month ==2 ) $check = 0;
          if($month == 2 && $day == 29 && $year % 4 != 0 ) $check = 0;
          if($month == 2 && $day == 29 && $year%100 ==0 && $year % 400 !=0) $check = 0;
          return $check;
        } 
        function days_of_month($month,$year){
          switch($month){
            case 1: case 3: case 5: case 7: case 8: case 10: case 12:
              return 31;
            case 4: case 6: case 9: case 11:
              return 30;
            case 2:
              if($year % 4 == 0 && $year % 100 != 0){
                return 29;
              }
              elseif($year % 400 == 0){
                return 29;
              }
              else return 28;  
          }
        } 
         
        ?>
    </body>
</html>