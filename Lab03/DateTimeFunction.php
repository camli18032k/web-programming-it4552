<html>
    <body>
        <?php 
        $name_1 = $_POST["name_1"];
        $day_1 = $_POST["day_1"];
        $month_1 = $_POST["month_1"];
        $year_1 = $_POST["year_1"];
        $name_2 = $_POST["name_2"];
        $day_2 = $_POST["day_2"];
        $month_2 = $_POST["month_2"];
        $year_2 = $_POST["year_2"];
        ?>
        <form method = "post">
        <p>Enter username 1: <input type = "text" name = "name_1" value =<?php echo $name_1?>></p>
        <p>Enter user's date of birth:     
         <select name = "day_1">
         <?php for($i=1; $i<=31; $i++){
            if($i == $day_1) select_option($i);
            else  print_option($i);
         }   
         ?>
        </select>
        <select name= "month_1">
         <?php for($i=1; $i<=12; $i++){
              if($i == $month_1) select_option($i);
              else  print_option($i);
         }   
         ?>
        </select>
        <select name= "year_1">
         <?php for($i=1990; $i<=2021; $i++){
              if($i == $year_1) select_option($i);
              else print_option($i);
         }   
         ?>
        </select>     
        </p>
        <p>Enter username 2: <input type = "text" name = "name_2" value =<?php echo $name_2?>></p>
        <p>Enter user's date of birth:     
         <select name = "day_2">
         <?php for($i=1; $i<=31; $i++){
              if($i == $day_2) select_option($i);
              else   print_option($i);
         }   
         ?>
        </select>
        <select name= "month_2">
         <?php for($i=1; $i<=12; $i++){
              if($i == $month_2) select_option($i);
              else print_option($i);
         }   
         ?>
        </select>
        <select name= "year_2">
         <?php for($i=1990; $i<=2021; $i++){
             if($i == $year_2) select_option($i);
             else print_option($i);
         }   
         ?>
        </select>     
        </p>
        <input type="submit" value= "Submit">
        <input type= "reset" value= "Reset">
        </form>
        <p><?php print_birthday($name_1, $year_1, $month_1, $day_1)?></p>
        <p><?php print_birthday($name_2, $year_2, $month_2, $day_2)?></p>
        <p><?php
            print("$name_1's birthday is ");
            print_difference_days($day_1, $day_2, $month_1, $month_2, $year_1, $year_2);
            print(" away from $name_2's birthday") 
         ?>
        </p>
        <p>
            <?php
            $age_1 = calculate_age($year_1);
            if($age_1 <= 1){
            print("$name_1 is $age_1 year old");}
            else print("$name_1 is $age_1 years old");
            ?>
        </p>
        <p>
            <?php
            $age_2 = calculate_age($year_2);
            if($age_2 <= 1) print("$name_2 is $age_2 year old");
            else print("$name_2 is $age_2 years old");
            ?>
        </p>
        <p>
            <?php
            $year = calculate_two_persons_age($year_1, $year_2);
            if($year <= 1) print("$name_1 is $year year old away from $name_2");
            else print("$name_1 is $year years old away from $name_2");
            ?>
        </p>
        <?php 
        function print_option($ten_option){
            if($ten_option<10)  print("<option value = \"0$ten_option\">0$ten_option</option>");
            else                print("<option value = \"$ten_option\">$ten_option</option>");
        }
        function select_option($ten_option){
            if($ten_option<10)  print("<option value = \"0$ten_option\" selected>0$ten_option</option>");
            else                print("<option value = \"$ten_option\" selected>$ten_option</option>");
        } 
        function print_birthday($name, $year, $month, $day){
            print("$name's birthday is ");
            $date = date_create("$year-$month-$day");
            print(date_format($date, "l, F d, Y"));
        }
        function print_difference_days($day_1, $day_2, $month_1, $month_2, $year_1, $year_2){
            $date1 = "$year_1-$month_1-$day_1";
            $date2 = "$year_2-$month_2-$day_2";
            $diff = abs(strtotime($date2) - strtotime($date1));
            $years = floor($diff / (365*60*60*24));
            $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
            $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
            echo $years . " years, " . $months . " months, " . $days . " days";
        }
        function calculate_age($year){
            $year_now = date("Y");
            $age = $year_now - $year;
            return $age; 
        } 
        function calculate_two_persons_age($year1,$year2){
           $year = abs($year1 - $year2);
           return $year;
        }
        ?>       
    </body>
</html>