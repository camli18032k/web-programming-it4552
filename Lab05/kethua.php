<html>
    <body>
        <?php 
        class BaseClass {
            function __construct(){
                print "In BaseClass constructor\n";
            }
            function __destruct(){
                echo "BaseClass destruct function";
            }
        }
        class SubClass extends BaseClass {
            function __construct(){
                parent:: __construct();
                print "In SubClass constructor\n";
            }
             function __destruct(){
                 echo "SubClass destruct function";
             }
        }
        $obj = new BaseClass();
        $obj = new SubClass();
        ?>
    </body>
</html>